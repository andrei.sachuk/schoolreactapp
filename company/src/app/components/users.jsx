import React, { useEffect, useState } from "react";
import Pagination from "./pagination";
import { paginate } from "../utils/paginate";
import GroupList from "./groupList";
import api from "../api";
import SearchStatus from "./searchStatus";
import UsersTable from "./usersTable";
import _ from "lodash";
import { GridLoader } from "react-spinners";

const Users = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [professions, setProfessions] = useState();
  const [selectedProf, setSelectedProf] = useState();
  const [sortBy, setSortBy] = useState({ path: "name", order: "asc" });
  const [users, setUsers] = useState();
  useEffect(() => {
    api.users.fetchAll().then(setUsers);
  }, []);
  const handleDelete = (userId) => {
    setUsers(users.filter((user) => user._id !== userId));
  };
  const handleToggleBookMark = (id) => {
    setUsers(
      users.map((user) => {
        if (user._id === id) {
          return { ...user, bookmark: !user.bookmark };
        }
        return user;
      })
    );
  };
  const pageSize = 2;
  const handlePageChange = (pageIndex) => setCurrentPage(pageIndex);

  const handleProfessionSelect = (selectedProf) =>
    setSelectedProf(selectedProf);

  const handleSort = (item) => {
    setSortBy(item);
  };

  useEffect(() => {
    api.professions.fetchAll().then(setProfessions);
  }, []);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedProf]);

  if (!users)
    return (
      <div
        style={{
          width: "100vw",
          height: "100vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <GridLoader color="#3d5fc7" loading size={50} />
      </div>
    );

  const filteredUsers = selectedProf
    ? users.filter(
        (user) =>
          JSON.stringify(user.profession) === JSON.stringify(selectedProf)
      )
    : users;
  const countUsers = filteredUsers.length;

  const sortedUsers = _.orderBy(filteredUsers, [sortBy.path], [sortBy.order]);

  (currentPage - 1) * pageSize >= filteredUsers.length &&
    setCurrentPage(currentPage - 1);
  const usersCrop = paginate(sortedUsers, currentPage, pageSize);

  const clearFilter = () => {
    setSelectedProf(null);
  };

  return (
    <div className="container-fluid p-4 ">
      <div className="d-flex justify-content-center">
        {professions && (
          <div className="d-flex flex-column flex-shrink-0 p-3">
            <GroupList
              selectedItem={selectedProf}
              onItemSelected={handleProfessionSelect}
              items={professions}
            />
            <button onClick={clearFilter} className="btn btn-secondary mt-2">
              {" "}
              Очистить
            </button>
          </div>
        )}
        <div className="d-flex flex-column">
          <SearchStatus length={countUsers} />
          {!!countUsers && (
            <UsersTable
              onSort={handleSort}
              users={usersCrop}
              onDelete={handleDelete}
              onToggleBookMark={handleToggleBookMark}
              selectedSort={sortBy}
            />
          )}
          <div className="d-flex justify-content-center mt-3">
            <Pagination
              itemsCount={countUsers}
              onPageChange={handlePageChange}
              currentPage={currentPage}
              pageSize={pageSize}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Users;

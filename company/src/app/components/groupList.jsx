import React from "react";
import * as PropTypes from "prop-types";

const GroupList = ({
  items,
  valueProperty,
  contentProperty,
  onItemSelected,
  selectedItem
}) => {
  if (!Array.isArray(items)) {
    return (
      <ul className="list-group">
        {Object.keys(items).map((key) => (
          <li
            key={items[key][valueProperty]}
            className={
              "list-group-item" + (selectedItem === items[key] ? " active" : "")
            }
            onClick={() => onItemSelected(items[key])}
            role="button"
          >
            {items[key][contentProperty]}
          </li>
        ))}
      </ul>
    );
  }
  return (
    <ul className="list-group">
      {items.map((item) => (
        <li
          key={item[valueProperty]}
          className={
            "list-group-item" + (selectedItem === item ? " active" : "")
          }
          onClick={() => onItemSelected(item)}
          role="button"
        >
          {item[contentProperty]}
        </li>
      ))}
    </ul>
  );
};

GroupList.defaultProps = {
  valueProperty: "_id",
  contentProperty: "name"
};

GroupList.propTypes = {
  items: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  valueProperty: PropTypes.string.isRequired,
  contentProperty: PropTypes.string.isRequired,
  onItemSelected: PropTypes.func.isRequired,
  selectedItem: PropTypes.object
};

export default GroupList;

import React from "react";
import PropTypes from "prop-types";

const TableHeader = ({ onSort, selectedSort, columns }) => {
  const handleSort = (sortColumn) => {
    onSort({
      path: sortColumn,
      order: selectedSort.order === "asc" ? "desc" : "asc"
    });
  };

  function renderSortArrow(selectedSort, currentPath) {
    if (selectedSort.path !== currentPath) {
      return null;
    }
    return selectedSort.order === "asc" ? (
      <i className="bi bi-caret-down-fill"></i>
    ) : (
      <i className="bi bi-caret-up-fill"></i>
    );
  }

  return (
    <thead>
      <tr>
        {Object.keys(columns).map((column) => (
          <th
            key={column}
            onClick={
              columns[column].path
                ? () => handleSort(columns[column].path)
                : undefined
            }
            {...{ role: columns[column].path && "button" }}
            scope="col"
            className="text-center"
          >
            {columns[column].name}{" "}
            {renderSortArrow(selectedSort, columns[column].path)}
          </th>
        ))}
      </tr>
    </thead>
  );
};

TableHeader.propTypes = {
  onSort: PropTypes.func.isRequired,
  selectedSort: PropTypes.object.isRequired,
  columns: PropTypes.object.isRequired
};

export default TableHeader;

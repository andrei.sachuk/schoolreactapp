import React from "react";
import _ from "lodash";
import * as PropTypes from "prop-types";
import { func } from "prop-types";

const Pagination = ({ itemsCount, pageSize, onPageChange, currentPage }) => {
  const pagesCount = Math.ceil(itemsCount / pageSize);
  if (pagesCount === 1) return null;
  const pages = _.range(1, pagesCount + 1);

  return (
    <nav>
      <ul className="pagination">
        <li className={"page-item" + (currentPage === 1 ? " disabled" : "")}>
          <a
            className="page-link"
            onClick={() => onPageChange(currentPage - 1)}
          >
            Previous
          </a>
        </li>
        {pages.map((page) => (
          <li
            className={"page-item" + (page === currentPage ? " active" : "")}
            onClick={() => onPageChange(page)}
            role="button"
            key={"page" + page}
          >
            <a className="page-link">{page}</a>
          </li>
        ))}
        <li
          className={
            "page-item" + (currentPage === pagesCount ? " disabled" : "")
          }
        >
          <a
            className="page-link"
            onClick={() => onPageChange(currentPage + 1)}
          >
            Next
          </a>
        </li>
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: func.isRequired
};

export default Pagination;

import users from "./fake.api/user.api.js";
import professions from "./fake.api/prof.api.js";

const API = {
  users,
  professions
};
export default API;
